# GeoWiki Project

![picture](img/AppLayout.png)

## Summary
GeoWiki is an App for travelers who like to read. You can open the App to see the Wikipedia articles about places that are close to your current location.

## Functionality
- Main page should display map with current user location
- Main page is optimized for Landscape and Portrait orientation
- When the App starts Wikipedia articles for the current location get loaded and displayed in a list
- Additionally there is a scrollable MapKit map that dislays the locations of the Wikipedia articles as pins (non-interactive)
- The list elements always show the current distance from the user to the place of interest
- Whenever the user moves for more than 100 meters the list should get refreshed

## APIs
Use Wikipedia API to query close by articles:

https://en.wikipedia.org/w/api.php?action=query&list=geosearch&gsradius=10000&gscoord=37.786971|-122.399677&format=json